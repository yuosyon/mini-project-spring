package com.article.articles.services;

import com.article.articles.models.Category;

import java.util.List;

public interface CategoryService {
    List<Category> findAllCategory();
    Category findOneCategory(int id);
}
