package com.article.articles.services;

import com.article.articles.models.Article;

import java.util.List;

public interface ArticleService {
    void insert(Article article);
    Article findOne(int id);
    List<Article> findAll();
    void delete(int id);
    void update(Article article);
}
