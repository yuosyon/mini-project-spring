package com.article.articles.services;

import com.article.articles.models.Article;
import com.article.articles.repositorys.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceIm implements ArticleService{
    private ArticleRepository articleRepository;
    @Autowired
    public void setArticleRepository(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public void insert(Article article) {
        articleRepository.insert(article);
    }

    @Override
    public Article findOne(int id) {
        return articleRepository.findOne(id);
    }

    @Override
    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    @Override
    public void delete(int id) {
        articleRepository.delete(id);
    }

    @Override
    public void update(Article article) {
        articleRepository.update(article);
    }
}
