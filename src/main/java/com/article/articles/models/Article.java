package com.article.articles.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Article {
    private int id;
    @NotEmpty(message = "please input character from 5 to 50")
    private String title;
    @NotEmpty(message = "please input character from 10 to 100")
    private String decription;
    private String thumbnail;
    private Category category;
    private String createdDate;
    @NotEmpty(message = "please input character from 5 to 30")
    private String author;

    public Article() {
    }

    public Article(int id, String title, String decription,String author,String thumbnail,String createdDate) {
        this.id = id;
        this.title = title;
        this.decription = decription;
        this.author = author;
        this.thumbnail=thumbnail;
        this.createdDate = createdDate;

    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDecription() {
        return decription;
    }

    public void setDecription(String decription) {
        this.decription = decription;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getAuthor() {
        return author;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", decription='" + decription + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", category=" + category +
                ", createdDate='" + createdDate + '\'' +
                ", author='" + author + '\'' +
                '}';
    }
}
