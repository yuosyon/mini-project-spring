package com.article.articles.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfiguration {
    @Bean
    @Profile("KpsArticle") //use when have many data source
    @Description("Replace DataSource in application.properties")
    public DataSource dataSource(){
        DriverManagerDataSource db = new DriverManagerDataSource();
        db.setDriverClassName("org.postgresql.Driver");
        db.setUrl("jdbc:postgresql://127.0.0.1:5432/kps_article");
        db.setUsername("postgres");
        db.setPassword("12345");
        return db;
    }

    //In memory
    @Bean
    @Profile("memoryDatabase")
    public DataSource inMemory(){
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        builder.setType(EmbeddedDatabaseType.H2);
        builder.addScripts("Sql/tables.sql",  "Sql/Data.sql");

        return builder.build();
    }
}
