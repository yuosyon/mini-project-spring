package com.article.articles.controllers;

import ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy;
import com.article.articles.models.Article;
import com.article.articles.services.ArticleService;
import com.article.articles.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
public class Articlecontroller {
    private ArticleService articleService;
    private CategoryService categoryService;
    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }
    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/article")
    public String article(Model model){
        List<Article> articles=articleService.findAll();
        model.addAttribute("articles",articles);
        return "article";
    }
    @GetMapping("/add")
    public String addArticle( Model model){
        model.addAttribute("formAdd",true);
        model.addAttribute("categories",categoryService.findAllCategory());
        model.addAttribute("article",new Article());
        return "add";
    }
    @PostMapping("/add")
    public String saveArticle(@Valid Article article, BindingResult bindingResult, MultipartFile file,Model model){
       if (file==null){
           return null;
       }
        File path=new File("/images");
       if (!path.exists())
           path.mkdir();
        if (bindingResult.hasErrors()){
            model.addAttribute("formAdd",true);
            model.addAttribute("categories",categoryService.findAllCategory());
            return "add";
        }
        if(!file.isEmpty()){
            String fileName=file.getOriginalFilename();
            try {
                fileName= UUID.randomUUID()+"."+fileName.substring(fileName.lastIndexOf('.'));
                Files.copy(file.getInputStream(),Paths.get("/images/",fileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
            article.setThumbnail("/image/"+fileName);
        }
        else
            article.setThumbnail("/image/"+"a.jpg");
        article.setCategory(categoryService.findOneCategory(article.getCategory().getId()));
       article.setCreatedDate(new Date().toString());
        articleService.insert(article);
        return "redirect:/article";
    }
    @GetMapping("/article/{id}")
    public String findeOne(@PathVariable("id") int id, Model model){
        model.addAttribute("articles",articleService.findOne(id));
        return "article_detail";
    }
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id")int id){
        articleService.delete(id);
        return "redirect:/article";
    }
    String oldFile="";
    @GetMapping("/update/{id}")
    public String update(@PathVariable("id") int id, Model model){
        model.addAttribute("formAdd",false);
        model.addAttribute("categories",categoryService.findAllCategory());
        model.addAttribute("article",articleService.findOne(id));
        Article a = articleService.findOne(id);
        oldFile=a.getThumbnail();
        return "add";
    }
    @PostMapping("/update")
    public String saveChange(@ModelAttribute Article article,MultipartFile file){
        if (file.isEmpty()) {
            article.setCreatedDate(new Date().toString());
            article.setThumbnail(oldFile);
        }else {
            String fileName = file.getOriginalFilename();

            String extention = fileName.substring(fileName.lastIndexOf('.'));
            fileName = UUID.randomUUID() + "." + extention;
            try {
                Files.copy(file.getInputStream(), Paths.get("/images/", fileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
            article.setThumbnail("/image/" + fileName);
        }
        article.setCategory(categoryService.findOneCategory(article.getCategory().getId()));
        articleService.update(article);
        return "redirect:/article";
    }
}
