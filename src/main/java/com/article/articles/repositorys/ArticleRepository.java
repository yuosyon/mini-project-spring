package com.article.articles.repositorys;

import com.article.articles.models.Article;
import org.apache.ibatis.annotations.*;
import org.hibernate.validator.constraints.Range;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ArticleRepository {
    @Insert("INSERT INTO tb_article(title,description,author,thumbnail,created_date,category_id) " +
            "VALUES(#{title},#{decription},#{author},#{thumbnail},#{createdDate},#{category.id})")
    void insert(Article article);
    @Select("SELECT a.id, a.title,a.description,a.author,a.thumbnail," +
            "a.created_date,a.category_id,c.name FROM tb_article a" +
            " INNER JOIN tb_categorys as c ON a.category_id=c.id WHERE a.id=#{id}")
    @Results({
            @Result(property = "id",column = "id"),
            @Result(property = "title",column = "title"),
            @Result(property = "decription",column = "description"),
            @Result(property = "author",column = "author"),
            @Result(property = "thumbnail",column = "thumbnail"),
            @Result(property = "category.id",column = "id"),
            @Result(property = "category.name",column = "name"),
            @Result(property = "createdDate",column = "created_date")
    })
    Article findOne(int id);

    @Select("SELECT a.id ,a.title,a.description,a.author,a.thumbnail," +
            "a.created_date,a.category_id,c.name FROM tb_article a" +
            " INNER JOIN tb_categorys c ON a.category_id=c.id ")
    @Results({
            @Result(property = "id",column = "id"),
            @Result(property = "title",column = "title"),
            @Result(property = "decription",column = "description"),
            @Result(property = "author",column = "author"),
            @Result(property = "thumbnail",column = "thumbnail"),
            @Result(property = "createdDate",column = "created_date"),
            @Result(property = "category.id",column = "id"),
            @Result(property = "category.name",column = "name"),
    })
    List<Article> findAll();
    @Delete("delete from tb_article where id=#{id}")
    void delete(int id);
    @Update("UPDATE tb_article SET title=#{title}, description=#{decription}, author=#{author}," +
            "thumbnail=#{thumbnail}, category_id=#{category.id} where id=#{id}")
    void update(Article article);
}
