package com.article.articles.repositorys;

import com.article.articles.models.Article;
import com.article.articles.models.Category;
import com.article.articles.services.CategoryService;
import com.github.javafaker.Book;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
//@Repository
public class ArticleRepositoryIm implements ArticleRepository {
    private List<Article> articleList=new ArrayList<>();
    private CategoryService categoryService;
    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public ArticleRepositoryIm() {
//        Faker faker=new Faker();
//        for (int i=0 ;i<=2;i++){
//            articleList.add(new Article(i,faker.book().title(),
//                    faker.book().title(),
//                    faker.book().author(),faker.book().publisher(),faker.internet().image(),
//                    new Date().toString()));
//        }
    }

    @Override
    public void insert(Article article) {
       articleList.add(article);
    }

    @Override
    public Article findOne(int id) {
        for(Article a:articleList){
            if (a.getId()==id){
                return a;
            }
        }
        return null;
    }

    @Override
    public List<Article> findAll() {
        return articleList;
    }

    @Override
    public void delete(int id) {
        for (Article a:articleList){
            if (a.getId()==id){
                articleList.remove(a);
                return;
            }
        }
    }

    @Override
    public void update(Article article) {
        for (int i=0;i<articleList.size();i++){
            if (articleList.get(i).getId()==article.getId()){
                articleList.get(i).setTitle(article.getTitle());
                articleList.get(i).setCategory(article.getCategory());
                articleList.get(i).setDecription(article.getDecription());
                articleList.get(i).setAuthor(article.getAuthor());
                if(article.getThumbnail()!=null){
                    articleList.get(i).setThumbnail(article.getThumbnail());
                }
            }
        }
    }
}
