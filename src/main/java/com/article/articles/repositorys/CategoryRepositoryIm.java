package com.article.articles.repositorys;

import com.article.articles.models.Category;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
@Repository
public class CategoryRepositoryIm implements CategoryRepository{
    private List<Category> categories=new ArrayList<>();
    public CategoryRepositoryIm(){
        categories.add(new Category(1,"iphone"));
        categories.add(new Category(2,"Samsong"));
        categories.add(new Category(3,"LG"));
        categories.add(new Category(4,"Lumia"));
        categories.add(new Category(5,"Huwia"));
    }
    @Override
    public List<Category> findAllCategory() {
        return categories;
    }

    @Override
    public Category findOneCategory(int id) {
        for (Category a:categories){
            if (a.getId()==id)
                return a;
        }
        return null;
    }
}
