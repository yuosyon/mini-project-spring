package com.article.articles.repositorys;

import com.article.articles.models.Category;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface CategoryRepository {
    @Select("Select * from tb_categorys")
    @Results({
            @Result(property = "id",column = "id"),
            @Result(property = "name",column = "name")
    })
    List<Category> findAllCategory();
    @Select("Select id,name from tb_categorys where id=#{id}")
    @Results({
            @Result(property = "id",column = "id"),
            @Result(property = "name",column = "name")
    })
    Category findOneCategory(int id);
}
