CREATE TABLE tb_categorys(
id int PRIMARY KEY auto_increment,
name VARCHAR NOT NULL
);
create table tb_article(
id int  primary key auto_increment,
title varchar not null,
description text not null,
author varchar not null,
thumbnail varchar ,
created_date varchar not null,
category_id int references tb_categorys(id)
);